package ua.service;

import ua.dao.RenterDao;
import ua.dao.SomeForRentDao;

public interface AbstractFactory {

	RenterDao getRenterDao();
	
	SomeForRentDao getSomeForRentDao();
}
