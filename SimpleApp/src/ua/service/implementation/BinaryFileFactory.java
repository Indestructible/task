package ua.service.implementation;

import ua.dao.RenterDao;
import ua.dao.SomeForRentDao;
import ua.dao.implementation.RenterDaoBinaryFile;
import ua.dao.implementation.SomeForRentDaoBinaryFile;
import ua.service.AbstractFactory;

public class BinaryFileFactory implements AbstractFactory{

	@Override
	public RenterDao getRenterDao() {
		return new RenterDaoBinaryFile();
	}

	@Override
	public SomeForRentDao getSomeForRentDao() {
		return new SomeForRentDaoBinaryFile();
	}
}
