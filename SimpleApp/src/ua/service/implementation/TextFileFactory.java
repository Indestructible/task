package ua.service.implementation;

import ua.dao.RenterDao;
import ua.dao.SomeForRentDao;
import ua.dao.implementation.RenterDaoTextFile;
import ua.dao.implementation.SomeForRentDaoTextFile;
import ua.service.AbstractFactory;

public class TextFileFactory implements AbstractFactory{
	
	@Override
	public RenterDao getRenterDao() {
		return new RenterDaoTextFile();
	}

	@Override
	public SomeForRentDao getSomeForRentDao() {
		return new SomeForRentDaoTextFile();
	}
}
