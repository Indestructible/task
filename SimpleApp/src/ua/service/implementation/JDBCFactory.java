package ua.service.implementation;

import ua.dao.RenterDao;
import ua.dao.SomeForRentDao;
import ua.dao.implementation.RenterDaoJDBC;
import ua.dao.implementation.SomeForRentDaoJDBC;
import ua.service.AbstractFactory;

public class JDBCFactory implements AbstractFactory{

	@Override
	public RenterDao getRenterDao() {
		return new RenterDaoJDBC();
	}

	@Override
	public SomeForRentDao getSomeForRentDao() {
		return new SomeForRentDaoJDBC();
	}

}
