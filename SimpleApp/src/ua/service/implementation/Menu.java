package ua.service.implementation;

import java.util.Scanner;

import ua.dao.RenterDao;
import ua.dao.SomeForRentDao;
import ua.persistence.Renter;
import ua.persistence.implementation.Book;
import ua.persistence.implementation.User;
import ua.service.AbstractFactory;

public class Menu {

	private RenterDao renterDao;
	
	private SomeForRentDao forRentDao;
	
	private Scanner sc = new Scanner(System.in);
	
	public void showMenu(){
		boolean isWork = true;
		while(isWork){
			System.out.println("1 - Users");
			System.out.println("2 - Books");
			System.out.println("3 - Rents");
			System.out.println("4 - Exit");
			int input = sc.nextInt();
			switch (input) {
			case 1:{
				menuForUsers();
				break;
			}
			case 2:{
				menuForBooks();
				break;
			}
			case 3:{
				menuForRent();
				break;
			}
			case 4:{
				isWork = false;
				sc.close();
				break;
			}
			}
		}
		
	}
	
	private void menuForRent() {
		System.out.println("1. Rent	a book");
		System.out.println("2. Return a book");
		System.out.println("3. Show	history	of rents for user");
		System.out.println("3. Show	history	of rents for book");
		int input = sc.nextInt();
		switch (input) {
		case 1:{
			rentBook();
			break;
		}
		case 2:{
			returnBook();
			break;
		}
		case 3:{
			showHistoryByUser();
			break;
		}
		case 4:{
			showHistoryByBook();
			break;
		}
		}
	}

	private void showHistoryByBook() {
		Book book = createBook();
		forRentDao.showHistory(book);
	}

	private void showHistoryByUser() {
		User user = createUser();
		renterDao.showHistory(user);
	}

	private void returnBook() {
		User user = createUser();
		Book book = createBook();
		forRentDao.returnRent(user, book);
	}

	private void rentBook() {
		User user = createUser();
		Book book = createBook();
		renterDao.setRent(user, book);
	}

	private void menuForBooks() {
		System.out.println("1. Create book");
		System.out.println("2. Delete book");
		System.out.println("3. Show list of all books");
		int input = sc.nextInt();
		switch (input) {
		case 1:{
			saveBook();
			break;
		}
		case 2:{
			deleteBook();
			break;
		}
		case 3:{
			forRentDao.showAll();
			break;
		}
		}
	}

	private void deleteBook() {
		Book book = createBook();
		forRentDao.delete(book);
	}

	private void saveBook() {
		Book book = createBook();
		forRentDao.save(book);
	}

	private void menuForUsers() {
		System.out.println("1. Create user");
		System.out.println("2. Delete user");
		System.out.println("3. Show list of all users");
		int input = sc.nextInt();
		switch (input) {
		case 1:{
			saveUser();
			break;
		}
		case 2:{
			deleteUser();
			break;
		}
		case 3:{
			renterDao.showAll();
			break;
		}
		}
	}
	
	private void deleteUser() {
		User user = createUser();
		renterDao.delete(user);
	}

	private void saveUser(){
		User user = createUser();
		renterDao.save(user);
	}
	
	private Book createBook(){
		System.out.println("Enter book identifier");
		String identifier = sc.next();
		System.out.println("Enter book name");
		String name = sc.next();
		return new Book(identifier, name);
	}
	
	private User createUser(){
		System.out.println("Enter user identifier");
		String identifier = sc.next();
		System.out.println("Enter user name");
		String name = sc.next();
		return new User(identifier, name);
	}

	public void showSelectStoreMethod(){
		System.out.println("Select store method:");
		System.out.println("1 - for store to DB");
		System.out.println("2 - for store to text file");
		System.out.println("3 - for store to binary file");
		int select = sc.nextInt();
		AbstractFactory abstractFactory = selectStoreMethod(select);
		setStoreDao(abstractFactory);
	}
	
	public AbstractFactory selectStoreMethod(int select){
		switch (select) {
		case 1:
			return new JDBCFactory();
		case 2:
			return new TextFileFactory();
		case 3:
			return new BinaryFileFactory();
		default:
			throw new IllegalArgumentException("You must select 1 or 2 or 3");
		}
	}
	
	public void setStoreDao(AbstractFactory abstractFactory){
		renterDao = abstractFactory.getRenterDao();
		forRentDao = abstractFactory.getSomeForRentDao();
	}
}
