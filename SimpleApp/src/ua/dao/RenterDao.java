package ua.dao;

import java.util.List;

import ua.persistence.Renter;
import ua.persistence.SomeForRent;

public interface RenterDao {

	void save(Renter renter);
	
	void delete(Renter renter);
	
	void setRent(Renter renter, SomeForRent someForRent);
	
	List<Renter> showAll();
	
	List<SomeForRent> showHistory(Renter renter);
	
	
}
