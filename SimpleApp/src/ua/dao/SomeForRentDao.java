package ua.dao;

import java.util.List;

import ua.persistence.Renter;
import ua.persistence.SomeForRent;

public interface SomeForRentDao {

	void save(SomeForRent someForRent);
	
	void delete(SomeForRent someForRent);
	
	void returnRent(Renter renter, SomeForRent someForRent);
	
	List<SomeForRent> showAll();
	
	List<Renter> showHistory(SomeForRent someForRent);
}
