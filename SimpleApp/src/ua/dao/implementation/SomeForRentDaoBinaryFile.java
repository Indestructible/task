package ua.dao.implementation;

import java.util.List;

import ua.dao.SomeForRentDao;
import ua.persistence.Renter;
import ua.persistence.SomeForRent;

public class SomeForRentDaoBinaryFile implements SomeForRentDao{

	@Override
	public void save(SomeForRent someForRent) {
		System.out.println("Saved to binary file");
	}

	@Override
	public void delete(SomeForRent someForRent) {
		System.out.println("Deleted from binary file by "+someForRent.getIdentifier());
	}

	@Override
	public List<SomeForRent> showAll() {
		System.out.println("Shows all from binary file");
		return null;
	}

	@Override
	public List<Renter> showHistory(SomeForRent someForRent) {
		System.out.println("Sows all history from binary file by "+someForRent.getIdentifier());
		return null;
	}

	@Override
	public void returnRent(Renter renter, SomeForRent someForRent) {
		someForRent.setOwner(null);
		someForRent.setReturned(true);
		save(someForRent);
	}
}
