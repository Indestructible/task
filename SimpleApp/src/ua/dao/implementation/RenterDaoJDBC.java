package ua.dao.implementation;

import java.util.List;

import ua.dao.RenterDao;
import ua.persistence.Renter;
import ua.persistence.SomeForRent;

public class RenterDaoJDBC implements RenterDao{

	@Override
	public void save(Renter renter) {
		System.out.println("Saved to DB");
	}

	@Override
	public void delete(Renter renter) {
		System.out.println("Deleted from DB by "+renter.getIdentifier());
	}

	@Override
	public List<Renter> showAll() {
		System.out.println("Shows all from DB");
		return null;
	}

	@Override
	public List<SomeForRent> showHistory(Renter renter) {
		System.out.println("Shows all history from DB by "+renter.getIdentifier());
		return null;
	}

	@Override
	public void setRent(Renter renter, SomeForRent someForRent) {
		someForRent.setOwner(renter);
		someForRent.setReturned(false);
	}
}
