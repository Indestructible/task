package ua.dao.implementation;

import java.util.List;

import ua.dao.SomeForRentDao;
import ua.persistence.Renter;
import ua.persistence.SomeForRent;

public class SomeForRentDaoJDBC implements SomeForRentDao{

	@Override
	public void save(SomeForRent someForRent) {
		System.out.println("Saved to DB");
	}

	@Override
	public void delete(SomeForRent someForRent) {
		System.out.println("Deleted from DB by "+someForRent.getIdentifier());
	}

	@Override
	public List<SomeForRent> showAll() {
		System.out.println("Shows all from DB");
		return null;
	}

	@Override
	public List<Renter> showHistory(SomeForRent someForRent) {
		System.out.println("Sows all history from DB by "+someForRent.getIdentifier());
		return null;
	}

	@Override
	public void returnRent(Renter renter, SomeForRent someForRent) {
		someForRent.setOwner(null);
		someForRent.setReturned(true);
		save(someForRent);
	}

}
