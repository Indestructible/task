package ua.dao.implementation;

import java.util.List;

import ua.dao.RenterDao;
import ua.persistence.Renter;
import ua.persistence.SomeForRent;

public class RenterDaoTextFile implements RenterDao{

	@Override
	public void save(Renter renter) {
		System.out.println("Saved to text file");
	}

	@Override
	public void delete(Renter renter) {
		System.out.println("Deleted from text file by "+renter.getIdentifier());
	}

	@Override
	public List<Renter> showAll() {
		System.out.println("Shows all from text file");
		return null;
	}

	@Override
	public List<SomeForRent> showHistory(Renter renter) {
		System.out.println("Shows all history from text file by "+renter.getIdentifier());
		return null;
	}

	@Override
	public void setRent(Renter renter, SomeForRent someForRent) {
		someForRent.setOwner(renter);
		someForRent.setReturned(false);
	}
}
