package ua.dao.implementation;

import java.util.List;

import ua.dao.SomeForRentDao;
import ua.persistence.Renter;
import ua.persistence.SomeForRent;

public class SomeForRentDaoTextFile implements SomeForRentDao{

	@Override
	public void save(SomeForRent someForRent) {
		System.out.println("Saved to text file");
	}

	@Override
	public void delete(SomeForRent someForRent) {
		System.out.println("Deleted from text file by "+someForRent.getIdentifier());
	}

	@Override
	public List<SomeForRent> showAll() {
		System.out.println("Shows all from text file");
		return null;
	}

	@Override
	public List<Renter> showHistory(SomeForRent someForRent) {
		System.out.println("Sows all history from text file by "+someForRent.getIdentifier());
		return null;
	}

	@Override
	public void returnRent(Renter renter, SomeForRent someForRent) {
		someForRent.setOwner(null);
		someForRent.setReturned(true);
		save(someForRent);
	}

}
