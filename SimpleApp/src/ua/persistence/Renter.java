package ua.persistence;

import java.util.List;

public interface Renter {

	String getIdentifier();

	void setIdentifier(String identifier);

	List<SomeForRent> getListSomeForRent();

	void setListSomeForRent(List<SomeForRent> listSomeForRent);
}
