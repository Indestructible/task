package ua.persistence;

public interface SomeForRent {
	
	String getIdentifier();

	void setIdentifier(String identifier);
	
	Renter getOwner();
	
	void setOwner(Renter renter);
	
	boolean isReturned();

	void setReturned(boolean isReturned);
}
