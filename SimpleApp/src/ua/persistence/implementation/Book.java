package ua.persistence.implementation;

import ua.persistence.Renter;
import ua.persistence.SomeForRent;

public class Book implements SomeForRent{

	private String identifier;
	
	private String name;
	
	private Renter renter;
	
	private boolean isReturned;

	public Book() {
	}

	public Book(String identifier, String name) {
		this.name = name;
		this.identifier = identifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getIdentifier() {
		return identifier;
	}

	@Override
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	@Override
	public Renter getOwner() {
		return renter;
	}

	@Override
	public void setOwner(Renter renter) {
		this.renter = renter;
	}
	
	@Override
	public boolean isReturned() {
		return isReturned;
	}

	@Override
	public void setReturned(boolean isReturned) {
		this.isReturned = isReturned;
	}
}
