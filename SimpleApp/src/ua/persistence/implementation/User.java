package ua.persistence.implementation;

import java.util.ArrayList;
import java.util.List;

import ua.persistence.Renter;
import ua.persistence.SomeForRent;

public class User implements Renter{

	private String identifier;
	
	private String name;
	
	private List<SomeForRent> listSomeForRent = new ArrayList<SomeForRent>();

	public User() {
	}

	public User(String identifier, String name) {
		this.name = name;
		this.identifier = identifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getIdentifier() {
		return identifier;
	}

	@Override
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	@Override
	public List<SomeForRent> getListSomeForRent() {
		return listSomeForRent;
	}

	@Override
	public void setListSomeForRent(List<SomeForRent> listSomeForRent) {
		this.listSomeForRent = listSomeForRent;
	}
}
